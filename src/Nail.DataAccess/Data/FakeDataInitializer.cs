﻿using Nail.DataAccess.Abstractions;
using System;
using System.Collections.Generic;
using System.Text;

namespace Nail.DataAccess.Data
{
    public class FakeDataInitializer : IDbInitializer
    {
        public void InitializeDb()
        {
            FakeDataFactory.Start();
        }
    }
}
