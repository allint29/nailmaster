﻿using Nail.Core.Domain.Administrations;
using Nail.Core.Interfaces.IEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Nail.DataAccess.Data
{
    public static class FakeDataFactory
    {
        public static List<string> FirstNames = new List<string>()
        {
            "Abraham", "Addison", "Adrian", "Albert", "Alec", "Alfred", "Alvin", "Andrew", "Andy", "Archibald", "Archie", "Arlo", "Arthur", "Arthur", "Austen", "Barnabe", "Bartholomew", "Bertram", "Bramwell", "Byam", "Cardew", "Chad", "Chance", "Colin", "Coloman", "Curtis", "Cuthbert", "Daniel", "Darryl", "David", "Dickon", "Donald", "Dougie", "Douglas", "Earl", "Ebenezer", "Edgar", "Edmund", "Edward", "Edwin", "Elliot", "Emil", "Floyd", "Franklin", "Frederick", "Gabriel", "Galton", "Gareth", "George", "Gerard", "Gilbert", "Gorden", "Gordon", "Graham", "Grant", "Henry", "Hervey", "Hudson", "Hugh", "Ian", "Jack", "Jaime", "James", "Jason", "Jeffrey", "Joey", "John", "Jolyon", "Jonas", "Joseph", "Joshua", "Julian", "Justin", "Kurt", "Lanny", "Larry", "Laurence", "Lawton", "Lester", "Malcolm", "Marcus", "Mark", "Marshall", "Martin", "Marvin", "Matt", "Maximilian", "Michael", "Miles", "Murray", "Myron", "Nate", "Nathan", "Neil", "Nicholas", "Nicolas", "Norman", "Oliver", "Oscar", "Osric", "Owen", "Patrick", "Paul", "Peleg", "Philip", "Phillipps", "Raymond", "Reginald", "Rhys", "Richard", "Robert", "Roderick", "Rodger", "Roger", "Ronald", "Rowland", "Rufus", "Russell", "Sebastian", "Shahaf", "Simon", "Stephen", "Swaine", "Thomas", "Tobias", "Travis", "Victor", "Vincent", "Vincent", "Vivian", "Wayne", "Wilfred", "William", "Winston", "Zadoc"
        };

        public static List<string> LastNames = new List<string>()
        {
           "Иванов", "Смирнов", "Кузнецов", "Попов", "Васильев", "Петров", "Соколов", "Михайлов", "Новиков", "Федоров", "Морозов", "Волков", "Алексеев", "Лебедев", "Семенов", "Егоров", "Павлов", "Козлов", "Степанов", "Николаев", "Орлов", "Андреев", "Макаров", "Никитин", "Захаров", "Зайцев", "Соловьев", "Борисов", "Яковлев", "Григорьев", "Романов", "Воробьев", "Сергеев", "Кузьмин", "Фролов", "Александров", "Максимов", "Поляков", "Сорокин", "Виноградов", "Ковалев", "Белов", "Медведев", "Антонов", "Тарасов", "Жуков", "Баранов", "Филиппов", "Комаров", "Давыдов", "Беляев", "Герасимов", "Богданов", "Осипов", "Сидоров", "Матвеев", "Титов", "Марков", "Миронов", "Крылов", "Куликов", "Карпов", "Власов", "Мельников", "Денисов", "Гаврилов", "Тихонов", "Казаков", "Афанасьев"
        };

        public static List<string> EmailsList = new List<string>()
        {
            "ale@yandex.ru","bb@yandex.ru","rr@yandex.ru","ws@yandex.ru","rew@yandex.ru","rdt@yandex.ru","ry@yandex.ru",
        };

        public static List<int> YearsList = new List<int>();
        public static List<int> MonthList = new List<int>();
        public static List<int> DayList = new List<int>();

        public static List<DateTime> DateList = new List<DateTime>();

        public static ICollection<Employee> Employees { get; set; }

        public static ICollection<Role> Roles { get; set; }
        public static List<Guid> EmployeesKeys { get; set; } = new List<Guid>();
        public static List<Guid> RolesKeys { get; set; } = new List<Guid>();
        public static List<Guid> PreferencesKeys { get; set; } = new List<Guid>();
        public static List<Guid> PartnerPromoCodeLimitKeys { get; set; } = new List<Guid>();
        public static List<Guid> CustomersKeys { get; set; } = new List<Guid>();
        public static List<Guid> PartnersKeys { get; set; } = new List<Guid>();
        public static List<Guid> PromocodesKeys { get; set; } = new List<Guid>();
        static FakeDataFactory()
        {
            Start();
        }

        public static void Start()
        {

            for (int i = 0; i < 50; i++)
            {
                EmployeesKeys.Add(Guid.NewGuid());
                RolesKeys.Add(Guid.NewGuid());
                PreferencesKeys.Add(Guid.NewGuid());
                PartnerPromoCodeLimitKeys.Add(Guid.NewGuid());
                CustomersKeys.Add(Guid.NewGuid());
                PartnersKeys.Add(Guid.NewGuid());
                PromocodesKeys.Add(Guid.NewGuid());
            }

            for (int i = 1953; i < 2013; i++)
            {
                YearsList.Add(i);
            }

            for (int i = 1; i < 13; i++)
            {
                MonthList.Add(i);
            }

            for (int i = 1; i < 28; i++)
            {
                DayList.Add(i);
            }

            var rand = new Random();

            for (int i = 0; i < 50; i++)
            {

                DateTime date = new DateTime(YearsList[rand.Next(0, 59)], MonthList[rand.Next(0, 11)], DayList[rand.Next(0, 27)]);

                DateList.Add(date);
            }

            Roles = new List<Role>()
              {
              new Role()
              {
                  Id = RolesKeys[0],
                  Name = "Admin",
                  Description = "Администратор",
              },
              new Role()
              {
                  Id = RolesKeys[1],
                  Name = "PartnerManager",
                  Description = "Партнерский менеджер"
              }
           };

            var roles_list = Roles.ToList();

            Employees = new List<Employee>();

            for (int i = 0; i < 3; i++)
            {
                //var r1 = roles_list[0].MakeClone();
                //var r2 = roles_list[1].MakeClone();

                ;
                Employee empl = new Employee()
                {
                    

                    Id = EmployeesKeys[i],
                    FirstName = FirstNames[i],
                    LastName = LastNames[i],
                    Email = EmailsList[i],
                    RoleIds = new List<Guid>() { i < 1 ? roles_list[0].Id : roles_list[1].Id },
                    Roles = new List<Role>()// { i < 1 ?  r1: r2 }
                };

                Employees.Add(empl);
            }
        }
    }
}
