﻿using Nail.Core.Domain.Administrations;
using Nail.DataAccess.Abstractions;
using Nail.DataAccess.EntityFramework;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace Nail.DataAccess.Data
{
    public class EfDbInitializer : IDbInitializer
    {
        public EfDbInitializer(DataContext dataContext)
        {
            _dataContext = dataContext;
        }

        private DataContext _dataContext { get; }

        public async void InitializeDb()
        {
            // Убраля для миграции
            _dataContext.Database.EnsureDeleted();
            _dataContext.Database.EnsureCreated();

            FakeDataFactory.Start();

            _dataContext.AddRange(FakeDataFactory.Roles);
            _dataContext.AddRange(FakeDataFactory.Employees);

            _dataContext.SaveChanges();

            //var employers = await _dataContext.Employees.ToListAsync();
            //var roles = await _dataContext.Roles.ToListAsync();
            //
            //
            //for (int i=0; i<employers.Count; i++)
            //{
            //    if (i == 0)
            //        employers[i].Roles.Add(roles[0]);
            //    else
            //        employers[i].Roles.Add(roles[1]);
            //}
            //
            //_dataContext.SaveChanges();
            //
            ////_dataContext.AddRange(FakeDataFactory.Preferences);
            ////_dataContext.AddRange(FakeDataFactory.Customers);
            ////_dataContext.AddRange(FakeDataFactory.CustomersPreferences);
            ////_dataContext.AddRange(FakeDataFactory.PromoCodes);
            //
            //_dataContext.SaveChanges();
        }

    }
}
