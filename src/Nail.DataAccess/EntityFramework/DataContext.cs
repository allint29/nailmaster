﻿using Nail.Core.Domain.Administrations;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Nail.DataAccess.EntityFramework
{
    public class DataContext : DbContext
    {
        public DbSet<Role> Roles { get; set; }
        public DbSet<Employee> Employees { get; set; }

        public DataContext()
        {

        }

        public DataContext(DbContextOptions<DataContext> options)
            : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new EmployeeRoleConfiguration());
            modelBuilder
                .Entity<Employee>()
                .HasMany(c => c.Roles)
                .WithMany(p => p.Employees)
                .UsingEntity<EmployeeRole>
                (
                j => j
                    .HasOne(cp => cp.Role)
                    .WithMany(p => p.EmployeeRoles)
                    .HasForeignKey(cp => cp.RoleId),
                j => j
                    .HasOne(cp => cp.Employee)
                    .WithMany(c => c.EmployeeRoles)
                    .HasForeignKey(cp => cp.EmployeeId),
                j =>
                {
                    //j.Property(cp => cp.TimeStamp).HasDefaultValueSql("CURRENT_TIMESTAMP");
                    //j.Property(pt => pt.Mark).HasDefaultValue(3);
                    j.HasKey(cp => new { cp.EmployeeId, cp.RoleId });
                    j.ToTable("EmployeeRoles");
                }
             );


            //EFC5 Связь мн-ко-мн через промежуточную сущность
            //modelBuilder
            //    .Entity<Customer>()
            //    .HasMany(c => c.Preferences)
            //    .WithMany(p => p.Customers)
            //    .UsingEntity<CustomerPreference>
            //    (
            //    j => j
            //        .HasOne(cp => cp.Preference)
            //        .WithMany(p => p.CustomerPreferences)
            //        .HasForeignKey(cp => cp.PreferenceId),
            //    j => j
            //        .HasOne(cp => cp.Customer)
            //        .WithMany(c => c.CustomerPreferences)
            //        .HasForeignKey(cp => cp.CustomerId),
            //    j =>
            //    {
            //        j.Property(cp => cp.TimeStamp).HasDefaultValueSql("CURRENT_TIMESTAMP");
            //        //j.Property(pt => pt.Mark).HasDefaultValue(3);
            //        j.HasKey(cp => new { cp.CustomerId, cp.PreferenceId });
            //        j.ToTable("CustomerPreferences");
            //    }
            //);
        }

        public class EmployeeRoleConfiguration : IEntityTypeConfiguration<EmployeeRole>
        {
            public void Configure(EntityTypeBuilder<EmployeeRole> builder)
            {
                builder.HasKey(p => p.Id);
                builder.HasAlternateKey(p => new { p.RoleId, p.EmployeeId }).HasName("AlternativeKeyEmployeeRole");
                builder.HasIndex(i => new { i.RoleId, i.EmployeeId });
                

        
            }
        }
    }
}
