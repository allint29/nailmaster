﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Nail.DataAccess.Abstractions
{
    public interface IDbInitializer
    {
        public void InitializeDb();
    }
}
