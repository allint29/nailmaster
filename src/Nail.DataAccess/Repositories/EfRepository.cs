﻿using Nail.Core;
using Nail.Core.Domain.Administrations;
using Nail.Core.Interfaces.IHasProperty;
using Nail.Core.Interfaces.IRepositories;
using Nail.DataAccess.EntityFramework;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nail.DataAccess.Repositories
{
    public class EfRepository<T> :
        IRepository<T>
        where T : BaseEntity, IHasId
    {
        private readonly DataContext _dataContext;

        public EfRepository(DataContext dataContext)
        {
            _dataContext = dataContext;
        }

        public async Task AddAsync(T entity)
        {
            if (entity == null)
                return;

            await _dataContext.Set<T>().AddAsync(entity);
            await _dataContext.SaveChangesAsync();
        }

        public async Task AddRangeAsync(IEnumerable<T> entities)
        {


            await _dataContext.Set<T>().AddRangeAsync(entities);
            await _dataContext.SaveChangesAsync();
        }

        public async Task DeleteAsync(T entity)
        {
            _dataContext.Set<T>().Remove(entity);
            await _dataContext.SaveChangesAsync();
        }

        public async Task DeleteRangeAsync(IEnumerable<T> entities)
        {
            _dataContext.RemoveRange(entities);
            await _dataContext.SaveChangesAsync();
        }

        public async Task<IEnumerable<T>> GetAllAsync()
        {
            //return await _dataContext.Set<T>().ToListAsync();

            switch (typeof(T).Name)
            {
                case "Employee":
                    var result_e = await _dataContext.Set<Employee>().Include(e => e.Roles).ToListAsync();
                    if (result_e == null)
                        return null;

                    result_e.ForEach(e => {
                       e.RoleIds = new List<Guid>();
                       e.Roles?.ToList().ForEach(r => e.RoleIds.Add(r.Id));
                    });
                    return result_e as IEnumerable<T>;
            
            
                case "Role":
                    var result_r = await _dataContext.Set<Role>().Include(r => r.Employees).ToListAsync();
            
                    if (result_r == null)
                        return null;

                    result_r.ForEach(r =>
                    {
                        r.EmployeeIds = new List<Guid>();
                        r.Employees?.ToList().ForEach(e => r.EmployeeIds.Add(e.Id));
                    });

                    return result_r as IEnumerable<T>;
                default:
                    return await _dataContext.Set<T>().ToListAsync();
            }
        }

        public async Task<T> GetByIdAsync(Guid id) 
        {
            switch(typeof(T).Name)
            {
                case "Employee":
                    var result_e = await _dataContext.Set<Employee>().Include(e => e.Roles).FirstOrDefaultAsync(e => e.Id == id);
                    if (result_e == null)
                        return null;
                    result_e.RoleIds = new List<Guid>();
                    result_e.Roles?.ToList().ForEach(r => result_e.RoleIds.Add(r.Id));
                    return result_e as T;


                case "Role":
                    var result_r = await _dataContext.Set<Role>().Include(r => r.Employees).FirstOrDefaultAsync(r => r.Id == id);

                    if (result_r == null)
                        return null;
                    result_r.EmployeeIds = new List<Guid>();
                    result_r.Employees?.ToList().ForEach(e => result_r.EmployeeIds.Add(e.Id));
                    return result_r as T;
                default:
                    return await _dataContext.Set<T>().FirstOrDefaultAsync(e => e.Id == id);
            }
        }

        public async Task<IEnumerable<T>> GetRangeByIdsAsync(IEnumerable<Guid> ids)
        {
            return await _dataContext.Set<T>().Where(d => ids.Any(id => id == d.Id)).ToListAsync();
        }

        public async Task UpdateAsync(T entity)
        {
            _dataContext.Update(entity);
            await _dataContext.SaveChangesAsync();
        }

    }
}
