﻿using Nail.Core;
using Nail.Core.Interfaces.IHasProperty;
using Nail.Core.Interfaces.IRepositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nail.DataAccess.Repositories
{
    public class InMemoryRepository<T>
        : IRepository<T>
        where T : BaseEntity, IHasId
    {
        protected ICollection<T> Data { get; set; }

        public InMemoryRepository(ICollection<T> data)
        {
            Data = data.ToList();
        }

        public Task AddAsync(T entity)
        {
            if (entity == null)
                return Task.CompletedTask;

            Data.Add(entity);

            return Task.CompletedTask;
        }

        public Task AddRangeAsync(IEnumerable<T> entities)
        {
            entities.ToList().ForEach(e => Data.Add(e));

            return Task.CompletedTask;
        }

        public Task DeleteAsync(T entity)
        {
            var t = Data.FirstOrDefault(e => e.Id == entity.Id);
            if (t != null)
                Data.Remove(t);

            return Task.CompletedTask;
        }

        public Task DeleteRangeAsync(IEnumerable<T> entities) 
        {
            entities.ToList().ForEach(e => 
            {
                DeleteAsync(e);

            });

            return Task.CompletedTask;
        }

        public Task<IEnumerable<T>> GetAllAsync()
        {
            return Task.FromResult(Data.AsEnumerable());
        }

        public Task<T> GetByIdAsync(Guid id)
        {
            return Task.FromResult(Data.FirstOrDefault(x => x.Id == id));
        }

        public Task UpdateAsync(T entity)
        {
            var t = Data.FirstOrDefault(e => e.Id == entity.Id);
            if (t != null)
            {
                Data.Remove(t);
                Data.Add(entity);
            }

            return Task.CompletedTask;
        }

        public Task<IEnumerable<T>> GetRangeByIdsAsync(IEnumerable<Guid> ids)
        {
            return Task.FromResult(Data.Where(d => ids.Any(id => id == d.Id)).AsEnumerable());
        }
    }
}
