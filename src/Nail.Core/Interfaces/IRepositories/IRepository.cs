﻿using Nail.Core.Interfaces.IHasProperty;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Nail.Core.Interfaces.IRepositories
{
    public interface IRepository<T> where T 
        : BaseEntity, IHasId
    {
        public Task<IEnumerable<T>> GetAllAsync();
        public Task<T> GetByIdAsync(Guid id);
        public Task AddAsync(T entity);
        public Task AddRangeAsync(IEnumerable<T> entity);
        public Task UpdateAsync(T entity);
        public Task DeleteAsync(T entity);
        public Task DeleteRangeAsync(IEnumerable<T> entities);
        public Task<IEnumerable<T>> GetRangeByIdsAsync(IEnumerable<Guid> ids);
    }

}
