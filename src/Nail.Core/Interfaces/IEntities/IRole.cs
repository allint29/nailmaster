﻿using Nail.Core.Domain.Administrations;
using Nail.Core.Interfaces;
using Nail.Core.Interfaces.IHasProperty;
using Nail.Core.Interfaces.IMethods;
using System;
using System.Collections.Generic;
using System.Text;

namespace Nail.Core.Interfaces.IEntities
{
    public interface IRole : IHasId, IHasEmployees, ICopy<Role>
    {
        public string Name { get; set; }
        public string Description { get; set; }

    }
}
