﻿using Nail.Core.Interfaces.IHasProperty;
using System;
using System.Collections.Generic;
using System.Text;

namespace Nail.Core.Interfaces.IEntities
{
    public interface IEmployeeRole : 
        IHasId, 
        IHasEmployee, 
        IHasRole
    {
    }
}
