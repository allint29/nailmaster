﻿using Nail.Core.Domain.Administrations;
using Nail.Core.Interfaces;
using Nail.Core.Interfaces.IHasProperty;
using Nail.Core.Interfaces.IMethods;
using System;
using System.Collections.Generic;
using System.Text;

namespace Nail.Core.Interfaces.IEntities
{
    public interface IEmployee : IHasId, IHasRoles, ICopy<Employee>
    {
         public string FirstName { get; set; }
         public string LastName { get; set; }
         public string Email { get; set; }
    }
}
