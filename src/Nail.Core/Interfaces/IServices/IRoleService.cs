﻿using Nail.Core.Application.DtoModels.Employees;
using Nail.Core.Application.DtoModels.Roles;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Nail.Core.Interfaces.IServices
{
    public interface IRoleService
    {
        Task<List<RoleResponse>> GetRolesAsync();

        Task<RoleResponse> GetRoleByIdAsync(Guid role_id, bool with_employee = false);

        Task<IEnumerable<EmployeeResponse>> GetRoleEmployeesAsync(Guid role_id);

        Task<IEnumerable<RoleResponse>> GetRolesByRangeAsync(IEnumerable<Guid> roles_ids);

        Task<RoleResponse> UpdateRoleAsync(CreateOrEditRoleRequest role_request);

        Task<RoleResponse> DeleteRoleAsync(Guid role_id);

        Task DeleteRolesRangeAsync(IEnumerable<Guid> role_ids);

    }
}
