﻿using Nail.Core.Application.DtoModels.Employees;
using Nail.Core.Application.DtoModels.Roles;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Nail.Core.Interfaces.IServices
{
    public interface IEmployeeService
    {

        Task<List<EmployeeResponse>> GetEmployeesAsync();

        Task<EmployeeResponse> GetEmployeeByIdAsync(Guid employee_id, bool with_role = false);

        Task<IEnumerable<RoleResponse>> GetEmployeeRolesAsync(Guid employee_id);

        Task<EmployeeResponse> SetRoleToEmployeeAsync(Guid role_id, Guid employee_id);

        Task<EmployeeResponse> RemoveRoleFromEmployeeAsync(Guid role_id, Guid employee_id);

        Task<IEnumerable<EmployeeResponse>> GetEmployeeByRangeAsync(IEnumerable<Guid> employee_ids);

        Task<EmployeeResponse> UpdateEmployeeAsync(CreateOrEditEmployeeRequest employee_request);

        Task<EmployeeResponse> DeleteEmployeeAsync(Guid employee_id);

        Task DeleteEmployeesRangeAsync(IEnumerable<Guid> employee_ids);
    }
}
