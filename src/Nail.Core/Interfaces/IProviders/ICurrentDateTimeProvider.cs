﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Nail.Core.Interfaces.IProviders
{
    public interface ICurrentDateTimeProvider
    {
        DateTime CurrentDateTime { get; }
    }
}
