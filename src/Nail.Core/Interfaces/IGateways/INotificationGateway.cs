﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Nail.Core.Interfaces.IGateways
{
    public interface INotificationGateway
    {
        Task SendNotificationToEmployeeAsync(Guid employeeId, string message);
    }
}
