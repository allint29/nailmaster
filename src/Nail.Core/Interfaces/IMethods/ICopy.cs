﻿using Nail.Core.Interfaces.IHasProperty;
using System;
using System.Collections.Generic;
using System.Text;

namespace Nail.Core.Interfaces.IMethods
{
    public interface ICopy<T>
    {
        public T MakeClone();
    }
}
