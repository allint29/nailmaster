﻿using Nail.Core.Domain.Administrations;
using Nail.Core.Interfaces.IEntities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Nail.Core.Interfaces.IHasProperty
{
    /// <summary>
    /// Включает список id ролей и список самих ролей
    /// </summary>
    public interface IHasRoles
    {
        public ICollection<Guid> RoleIds { get; set; }
        public ICollection<Role> Roles { get; set; }
    }
}
