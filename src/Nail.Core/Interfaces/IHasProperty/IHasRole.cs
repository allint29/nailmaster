﻿
using Nail.Core.Domain.Administrations;
using Nail.Core.Interfaces.IEntities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Nail.Core.Interfaces.IHasProperty
{
    /// <summary>
    /// Включает id роли и саму роль
    /// </summary>
    public interface IHasRole
    {
        public Guid RoleId { get; set; }
        public Role Role { get; set; }

    }
}
