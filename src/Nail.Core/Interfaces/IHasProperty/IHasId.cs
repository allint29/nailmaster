﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Nail.Core.Interfaces.IHasProperty
{
    /// <summary>
    /// Поле Id Guid
    /// </summary>
    public interface IHasId
    {
        public Guid Id { get; set; }
    }
}
