﻿
using Nail.Core.Domain.Administrations;
using Nail.Core.Interfaces.IEntities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Nail.Core.Interfaces.IHasProperty
{
    /// <summary>
    /// Включает id работника и самого работника
    /// </summary>
    public interface IHasEmployee
    {
        public abstract Guid EmployeeId { get; set; }
        public abstract Employee Employee { get; set; }

    }
}
