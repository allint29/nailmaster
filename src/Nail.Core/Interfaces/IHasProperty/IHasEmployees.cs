﻿
using Nail.Core.Domain.Administrations;
using Nail.Core.Interfaces.IEntities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Nail.Core.Interfaces.IHasProperty
{
    /// <summary>
    /// Включает список id работников и список самих работников
    /// </summary>
    public interface IHasEmployees
    {
        public ICollection<Guid> EmployeeIds { get; set; }
        public ICollection<Employee> Employees { get; set; }
    }
}
