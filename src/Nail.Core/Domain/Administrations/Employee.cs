﻿using Nail.Core.Interfaces.IEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace Nail.Core.Domain.Administrations
{
    public class Employee : BaseEntity, IEmployee
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string FullName => $"{LastName} {FirstName}";
        public string Email { get; set; }

        [NotMapped]
        public ICollection<Guid> RoleIds { get; set; }
        public ICollection<Role> Roles { get; set; }

        [NotMapped]
        public ICollection<EmployeeRole> EmployeeRoles { get ; set;}


        public Employee MakeClone()
        {
            var result = new Employee {
                Id = this.Id,
                FirstName = this.FirstName,
                LastName = this.LastName,
                Email = this.Email,
                RoleIds = new List<Guid>(),
                Roles = new List<Role>()
                };
            this.RoleIds?.ToList().ForEach(id => result.RoleIds.Add(id));

            this.Roles?.ToList().ForEach(r => result.Roles.Add(new Role { Id = r.Id, Name = r.Name, Description = r.Description }));

            return result;
        }
    }
}
