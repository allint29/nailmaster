﻿using Nail.Core.Interfaces.IEntities;
using Nail.Core.Interfaces;
using Nail.Core.Interfaces.IHasProperty;
using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Nail.Core.Domain.Administrations
{
    public class EmployeeRole : BaseEntity, IEmployeeRole
    {
       // [Key]
       // [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
       // public Guid Id { get; set; }
        public Guid EmployeeId { get; set; }
        public Employee Employee { get; set; }
        public Guid RoleId { get; set; }
        public Role Role { get; set; }
    }
}
