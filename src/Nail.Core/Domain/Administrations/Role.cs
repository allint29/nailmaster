﻿using Nail.Core.Interfaces.IEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace Nail.Core.Domain.Administrations
{
    public class Role : BaseEntity, IRole
    {
        public string Name { get; set; }
        public string Description { get; set; }

        [NotMapped]
        public ICollection<Guid> EmployeeIds { get; set; }
        public ICollection<Employee> Employees { get; set; }

        [NotMapped]
        public ICollection<EmployeeRole> EmployeeRoles { get; set; }

        public Role MakeClone()
        {
            var result = new Role
            {
                Id = this.Id,
                Description = this.Description,
                Name = this.Name,
                EmployeeIds = new List<Guid>(),
                Employees = new List<Employee>()

            };

            this.EmployeeIds?.ToList().ForEach(id => 
            {
                ;
                result.EmployeeIds.Add(id);
            });

            this.Employees?.ToList().ForEach(e =>
            {
                result.Employees.Add(
                    new Employee
                    {
                        Id = this.Id,
                        FirstName = e.FirstName,
                        LastName = e.LastName,
                        Email = e.Email,
                        RoleIds = new List<Guid>(),
                        Roles = new List<Role>()
                    });
            });

            return result;
        }
    }
}
