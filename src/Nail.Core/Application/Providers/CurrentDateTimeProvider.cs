﻿using Nail.Core.Interfaces.IProviders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Nail.Core.Application.Providers
{
    public class CurrentDateTimeProvider
        : ICurrentDateTimeProvider
    {
        public DateTime CurrentDateTime => DateTime.Now;
    }

}
