﻿using Nail.Core.Domain.Administrations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Nail.Core.Application.DtoModels.Roles
{
    public class CreateOrEditRoleRequest
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public ICollection<Guid> EmployeeIds { get; set; }
        public ICollection<Employee> Employees { get; set; }
    }
}
