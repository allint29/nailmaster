﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Nail.Core.Application.DtoModels.Employees;

namespace Nail.Core.Application.DtoModels.Roles
{
    public class RoleResponse : RoleShortResponse
    {
        public List<Guid> EmployeeIds { get; set; }
        public List<EmployeeShortResponse> Employees { get; set; }

    }
}
