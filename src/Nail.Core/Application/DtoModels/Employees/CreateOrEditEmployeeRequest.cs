﻿using Nail.Core.Domain.Administrations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Nail.Core.Application.DtoModels.Employees
{
    public class CreateOrEditEmployeeRequest
    {
        public Guid Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public ICollection<Guid> RoleIds { get; set; }
        public ICollection<Role> Roles { get; set; }
        //public int AppliedPromocodesCount { get; set; }
    }

}
