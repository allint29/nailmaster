﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Nail.Core.Application.DtoModels.Roles;

namespace Nail.Core.Application.DtoModels.Employees
{
    public class EmployeeResponse : EmployeeShortResponse
    {
        public List<Guid> RoleIds { get; set; }
        public List<RoleShortResponse> Roles { get; set; }

        /// <summary>
        /// Имя сотрудника
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        /// Фамилия
        /// </summary>
        public string LastName { get; set; }

        //public int AppliedPromocodesCount { get; set; }

    }
}
