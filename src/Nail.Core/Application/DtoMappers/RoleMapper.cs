﻿using Nail.Core.Domain.Administrations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Nail.Core.Application.DtoModels.Employees;
using Nail.Core.Application.DtoModels.Roles;

namespace Nail.Core.Application.DtoMappers
{
    public class RoleMapper
    {
        public static Role MapToModel(CreateOrEditRoleRequest model, IEnumerable<Employee> employees, Role role = null)
        {
            if (role == null)
            {
                role = new Role
                {
                    Id = Guid.NewGuid()
                };
            }

            role.Name = model.Name;
            role.Description = model.Description;
            role.EmployeeIds = new List<Guid>();
            role.Employees = new List<Employee>();

            if (employees == null || employees.Count() == 0)
            {
                model.Employees?.ToList().ForEach(e =>
                {
                    role.EmployeeIds.Add(e.Id);
                    role.Employees.Add(e);
                });
            }
            else
            {
                employees?.ToList().ForEach(e =>
                {
                    role.EmployeeIds.Add(e.Id);
                    role.Employees.Add(e);
                });
            }

            return role;
        }


        /// <summary>
        /// Маппинг из модели в rest ответ
        /// </summary>
        /// <param name="role"></param>
        /// <param name="full_info"></param>
        /// <returns></returns>
        public static RoleShortResponse MapFromModel(Role role, bool full_info)
        {
            if (role == null)
                return null;

            RoleResponse result = new RoleResponse() { Id = role.Id, Name = role.Name, Description = role.Description };

            if (!full_info)
                return result;

            result.EmployeeIds = new List<Guid>();

            if (role.EmployeeIds?.Count > 0)
            {
                role.EmployeeIds.ToList().ForEach(id => result.EmployeeIds?.Add(id));
            }

            result.Employees = new List<EmployeeShortResponse>();

            if (role.Employees?.Count > 0)
            {
                role.Employees.ToList().ForEach(e =>
                {
                    result.Employees?.Add(new EmployeeShortResponse { Id = e.Id, FullName = e.FullName, Email = e.Email });
                });
            }

            return result;
        }

    }
}
