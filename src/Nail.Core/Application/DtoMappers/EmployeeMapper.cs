﻿using Nail.Core.Domain.Administrations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Nail.Core.Application.DtoModels.Employees;
using Nail.Core.Application.DtoModels.Roles;

namespace Nail.Core.Application.DtoMappers
{
    public class EmployeeMapper
    {
        /// <summary>
        /// Маппинг из rest запроса в модель работника
        /// </summary>
        /// <param name="model"></param>
        /// <param name="employee"></param>
        /// <returns></returns>
        public static Employee MapToModel(CreateOrEditEmployeeRequest model, IEnumerable<Role> roles, Employee employee = null)
        {
            if (employee == null)
            {
                employee = new Employee
                {
                    Id = Guid.NewGuid()
                };
            }

            employee.FirstName = model.FirstName;
            employee.LastName = model.LastName;
            employee.Email = model.Email;
            //employee.AppliedPromocodesCount = model.AppliedPromocodesCount;

            employee.RoleIds = new List<Guid>();
            employee.Roles = new List<Role>();

            if (roles == null || roles.Count() == 0)
            {
                model.Roles?.ToList().ForEach(r =>
                {
                    employee.RoleIds.Add(r.Id);
                    employee.Roles.Add(r);
                });
            }
            else
            {
                roles.ToList().ForEach(r =>
                {
                    employee.RoleIds.Add(r.Id);
                    employee.Roles.Add(r);
                });
            }


            return employee;
        }

        /// <summary>
        /// Маппинг из модели в rest ответ
        /// </summary>
        /// <param name="employee"></param>
        /// <param name="full_info"></param>
        /// <returns></returns>
        public static EmployeeShortResponse MapFromModel(Employee employee, bool full_info)
        {
            if (employee == null)
                return null;

            EmployeeResponse result = new EmployeeResponse()
            {
                FirstName = employee.FirstName,
                LastName = employee.LastName,
                Id = employee.Id, 
                FullName = employee.FullName, 
                Email = employee.Email
            };

            if (!full_info)
                return result;

            result.RoleIds = new List<Guid>();

            if (employee.RoleIds?.Count > 0)
            {
                employee.RoleIds.ToList().ForEach(id => result.RoleIds?.Add(id));
            }

            result.Roles = new List<RoleShortResponse>();

            if (employee.Roles?.Count > 0)
            {
                employee.Roles.ToList().ForEach(r =>
                {
                    result.Roles?.Add(new RoleShortResponse { Id = r.Id, Name = r.Name, Description = r.Description });
                });
            }

            return result;
        }

        public static ICollection<EmployeeResponse> MapFromModelListToList(ICollection<Employee> list)
        {
            List<EmployeeResponse> result = new List<EmployeeResponse>();

            list.ToList().ForEach(e =>
            {
                var unit_response = MapFromModel(e, true);
                if (unit_response != null)
                    result.Add((EmployeeResponse)unit_response);
            });

            return result;
        }
    }
}
