﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Nail.Core.Application.Exceptions
{
    public class EntityNotFoundException
        : Exception
    {
        public EntityNotFoundException()
        {

        }

        public EntityNotFoundException(Guid entityId)
            : base($"Сущность с id {entityId} не найдена")
        {

        }

        public EntityNotFoundException(string message)
            : base(message)
        {

        }

        public EntityNotFoundException(Guid entityId, string message)
        : base($"Сущность с id {entityId} не найдена. Доп. информация: {message}")
        {

        }

        public EntityNotFoundException(string message, Exception exception)
            : base(message, exception)
        {

        }
    }
}
