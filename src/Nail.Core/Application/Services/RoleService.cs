﻿using Nail.Core.Application.DtoModels.Employees;
using Nail.Core.Application.DtoModels.Roles;
using Nail.Core.Domain.Administrations;
using Nail.Core.Interfaces.IRepositories;
using Nail.Core.Interfaces.IServices;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Linq;
using Nail.Core.Application.DtoMappers;
using Nail.Core.Application.Exceptions;

namespace Nail.Core.Application.Services
{
    public class RoleService
        : IRoleService
    {

        private readonly IRepository<Role> _roleRepository;
        private readonly IRepository<Employee> _employeeRepository;

        public RoleService(IRepository<Role> roleRepository, IRepository<Employee> employeeRepository)
        {
            _roleRepository = roleRepository;
            _employeeRepository = employeeRepository;
        }

        public async Task DeleteRolesRangeAsync(IEnumerable<Guid> role_ids)
        {
            var roles = await _roleRepository.GetRangeByIdsAsync(role_ids);
            await _roleRepository.DeleteRangeAsync(roles);
        }

        public async Task<RoleResponse> DeleteRoleAsync(Guid role_id)
        {
            var role = await _roleRepository.GetByIdAsync(role_id);

            if (role == null)
                throw new EntityNotFoundException(role_id);

            await _roleRepository.DeleteAsync(role);

            var result = RoleMapper.MapFromModel(role, true);

            return (RoleResponse)result;
        }

        public async Task<RoleResponse> GetRoleByIdAsync(Guid role_id, bool with_employee = false)
        {
            var role = await _roleRepository.GetByIdAsync(role_id);

            if (role == null)
                throw new EntityNotFoundException(role_id);

            var role_response = new RoleResponse
            {
                Id = role.Id,
                Name = role.Name,
                Description = role.Description,
                EmployeeIds = new List<Guid>(),
                Employees = new List<EmployeeShortResponse>()
            };


            if (!with_employee)
                return role_response;

            if (role.Employees?.Count > 0)
            {
                role.Employees.ToList().ForEach(e =>
                {
                    role.EmployeeIds.Add(e.Id);
                    role.Employees.Add(e);
                });
            }
            else if (role.EmployeeIds?.Count > 0)
            {
                var employees = await _employeeRepository.GetRangeByIdsAsync(role.EmployeeIds);
                employees.ToList().ForEach(e => role.Employees.Add(e));
            }

            return role_response;
        }

        public async Task<IEnumerable<EmployeeResponse>> GetRoleEmployeesAsync(Guid role_id)
        {
            var role = await _roleRepository.GetByIdAsync(role_id);

            if (role == null)
                throw new EntityNotFoundException(role_id);

            var employees = await _employeeRepository.GetAllAsync();
            var role_employees = employees.Where(e => e.Roles.Any(r => r.Id == role.Id));

            List<EmployeeResponse> result = new List<EmployeeResponse>();
            role_employees.ToList().ForEach(e => result.Add((EmployeeResponse)EmployeeMapper.MapFromModel(e, true)));

            return result;
        }

        public async Task<List<RoleResponse>> GetRolesAsync()
        {
            var roles = await _roleRepository.GetAllAsync();

            List<RoleResponse> roles_response = new List<RoleResponse>();

            roles.ToList().ForEach(r => roles_response.Add((RoleResponse)RoleMapper.MapFromModel(r, true)));

            return roles_response;
        }

        public async Task<IEnumerable<RoleResponse>> GetRolesByRangeAsync(IEnumerable<Guid> roles_ids)
        {
            var roles = await _roleRepository.GetRangeByIdsAsync(roles_ids);

            List<RoleResponse> result = new List<RoleResponse>();

            roles.ToList().ForEach(r => result.Add((RoleResponse)RoleMapper.MapFromModel(r, true)));

            return result;
        }

        public async Task<EmployeeResponse> RemoveRoleFromEmployeeAsync(Guid role_id, Guid employee_id)
        {
            var role = await _roleRepository.GetByIdAsync(role_id);

            var employee = await _employeeRepository.GetByIdAsync(employee_id);

            if (role == null)
                throw new EntityNotFoundException(role_id, "Роль не найдена по id");

            if (employee == null)
                throw new EntityNotFoundException(role_id, "Работник не найден по id");

            if (employee.Roles.Contains(role))
            {
                var r_id = employee.RoleIds.FirstOrDefault(id => id == role.Id);
                if (r_id != null && r_id != Guid.Empty)
                    employee.RoleIds.Remove(r_id);

                employee.Roles.Remove(role);
                await _employeeRepository.UpdateAsync(employee);
            }

            return (EmployeeResponse)EmployeeMapper.MapFromModel(employee, true);
        }

        public async Task<Guid> SetRoleToEmployeeAsync(Guid role_id, Guid employee_id)
        {

            var role = await _roleRepository.GetByIdAsync(role_id);

            var employee = await _employeeRepository.GetByIdAsync(employee_id);

            if (role == null)
                throw new EntityNotFoundException(role_id, "Роль не найдена по id");

            if (employee == null)
                throw new EntityNotFoundException(role_id, "Работник не найден по id");

            if (!employee.Roles.Contains(role))
            {
                employee.RoleIds.Add(role.Id);

                employee.Roles.Add(role);
                await _employeeRepository.UpdateAsync(employee);
            }

            return role.Id;
        }

        public async Task<RoleResponse> UpdateRoleAsync(CreateOrEditRoleRequest request)
        {
            List<Employee> employees = new List<Employee>();
            if (request?.EmployeeIds?.Count() > 0)
                employees.AddRange(await _employeeRepository.GetRangeByIdsAsync(request.EmployeeIds));

            Role existed_role = null;
            if (request.Id != Guid.Empty)
            {
                existed_role = await _roleRepository.GetByIdAsync(request.Id);

                if (existed_role != null)
                {
                    existed_role = RoleMapper.MapToModel(request, employees, existed_role);
                    await _roleRepository.UpdateAsync(existed_role);

                }
                else
                {
                    existed_role = RoleMapper.MapToModel(request, employees, existed_role);
                    await _roleRepository.AddAsync(existed_role);
                }
            }
            else
            {
                existed_role = RoleMapper.MapToModel(request, employees, existed_role);
                await _roleRepository.AddAsync(existed_role);
            }

            var result = RoleMapper.MapFromModel(existed_role, true);

            return (RoleResponse)result;
        }
    }
}
