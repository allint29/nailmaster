﻿using Nail.Core.Application.DtoMappers;
using Nail.Core.Application.DtoModels.Employees;
using Nail.Core.Application.DtoModels.Roles;
using Nail.Core.Application.Exceptions;
using Nail.Core.Domain.Administrations;
using Nail.Core.Interfaces.IGateways;
using Nail.Core.Interfaces.IRepositories;
using Nail.Core.Interfaces.IServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nail.Core.Application.Services
{
    public class EmployeeService
        : IEmployeeService
    {

        private readonly IRepository<Role> _roleRepository;
        private readonly IRepository<Employee> _employeeRepository;
        private readonly INotificationGateway _notification;

        public EmployeeService(IRepository<Role> roleRepository, 
            IRepository<Employee> employeeRepository,
            INotificationGateway notification)
        {
            _roleRepository = roleRepository;
            _employeeRepository = employeeRepository;
            _notification = notification;
        }

        public async Task<EmployeeResponse> DeleteEmployeeAsync(Guid employee_id)
        {
            var employee = await _employeeRepository.GetByIdAsync(employee_id);

            if (employee == null)
                throw new EntityNotFoundException(employee_id);            

            await _employeeRepository.DeleteAsync(employee);

            await _notification.SendNotificationToEmployeeAsync(employee_id, "Запись о работнике удалена.");
            SendEmail();
            var result = EmployeeMapper.MapFromModel(employee, true);

            return (EmployeeResponse)result;
        }

        public void SendEmail()
        {

        }

        public async Task DeleteEmployeesRangeAsync(IEnumerable<Guid> employee_ids)
        {
            var employees = await _employeeRepository.GetRangeByIdsAsync(employee_ids);
            await _employeeRepository.DeleteRangeAsync(employees);
        }

        public async Task<EmployeeResponse> GetEmployeeByIdAsync(Guid employee_id, bool with_role = false)
        {
            var employee = await _employeeRepository.GetByIdAsync(employee_id);

            if (employee == null)
                throw new EntityNotFoundException(employee_id);

            var employee_response = new EmployeeResponse
            {
                Id = employee.Id,
                FullName = employee.FullName,
                Email = employee.Email,
                RoleIds = new List<Guid>(),
                Roles = new List<RoleShortResponse>()
            };


            if (!with_role)
                return employee_response;

            if (employee.Roles?.Count > 0)
            {
                employee.Roles.ToList().ForEach(r =>
                {
                    employee.RoleIds.Add(r.Id);
                    employee.Roles.Add(r);
                });
            }
            else if (employee.RoleIds?.Count > 0)
            {
                var roles = await _roleRepository.GetRangeByIdsAsync(employee.RoleIds);
                roles.ToList().ForEach(r => employee.Roles.Add(r));
            }

            return employee_response;
        }

        public async Task<IEnumerable<EmployeeResponse>> GetEmployeeByRangeAsync(IEnumerable<Guid> employee_ids)
        {
            var employees = await _employeeRepository.GetRangeByIdsAsync(employee_ids);

            List<EmployeeResponse> result = new List<EmployeeResponse>();

            employees.ToList().ForEach(e => result.Add((EmployeeResponse)EmployeeMapper.MapFromModel(e, true)));

            return result;
        }

        public async Task<IEnumerable<RoleResponse>> GetEmployeeRolesAsync(Guid employee_id)
        {
            var roles = await _roleRepository.GetRangeByIdsAsync(new[] { employee_id });

            List<RoleResponse> roles_response = new List<RoleResponse>();

            roles.ToList().ForEach(r => roles_response.Add((RoleResponse)RoleMapper.MapFromModel(r, true)));

            return roles_response;
        }

        public async Task<List<EmployeeResponse>> GetEmployeesAsync()
        {
            var employees = await _employeeRepository.GetAllAsync();

            List<EmployeeResponse> employee_response = new List<EmployeeResponse>();

            employees.ToList().ForEach(e => employee_response.Add((EmployeeResponse)EmployeeMapper.MapFromModel(e, true)));

            return employee_response;
        }

        public async Task<EmployeeResponse> RemoveRoleFromEmployeeAsync(Guid role_id, Guid employee_id)
        {
            var role = await _roleRepository.GetByIdAsync(role_id);

            var employee = await _employeeRepository.GetByIdAsync(employee_id);

            if (role == null)
                throw new EntityNotFoundException(role_id, "Роль не найдена по id");

            if (employee == null)
                throw new EntityNotFoundException(role_id, "Работник не найден по id");

            if (employee.Roles.Contains(role))
            {
                var r_id = employee.RoleIds.FirstOrDefault(id => id == role.Id);
                if (r_id != null && r_id != Guid.Empty)
                    employee.RoleIds.Remove(r_id);

                employee.Roles.Remove(role);
                await _employeeRepository.UpdateAsync(employee);
            }

            return (EmployeeResponse)EmployeeMapper.MapFromModel(employee, true);

        }

        public async Task<EmployeeResponse> SetRoleToEmployeeAsync(Guid role_id, Guid employee_id)
        {

            var role = await _roleRepository.GetByIdAsync(role_id);

            var employee = await _employeeRepository.GetByIdAsync(employee_id);

            if (role == null)
                throw new EntityNotFoundException(role_id, "Роль не найдена по id");

            if (employee == null)
                throw new EntityNotFoundException(role_id, "Работник не найден по id");

            if (!employee.Roles.Contains(role))
            {
                employee.RoleIds.Add(role.Id);

                employee.Roles.Add(role);
                await _employeeRepository.UpdateAsync(employee);
            }

            var result = (EmployeeResponse)EmployeeMapper.MapFromModel(employee, true);

            return result;
        }

        public async Task<EmployeeResponse> UpdateEmployeeAsync(CreateOrEditEmployeeRequest request)
        {
            List<Role> roles = new List<Role>();
            if (request?.RoleIds?.Count() > 0)
                roles.AddRange(await _roleRepository.GetRangeByIdsAsync(request.RoleIds));


            Employee existed_employee = null;
            if (request.Id != Guid.Empty)
            {
                existed_employee = await _employeeRepository.GetByIdAsync(request.Id);

                if (existed_employee != null)
                {
                    existed_employee = EmployeeMapper.MapToModel(request, roles, existed_employee);
                    await _employeeRepository.UpdateAsync(existed_employee);

                }
                else
                {
                    existed_employee = EmployeeMapper.MapToModel(request, roles, existed_employee);
                    await _employeeRepository.AddAsync(existed_employee);
                }
            }
            else
            {
                existed_employee = EmployeeMapper.MapToModel(request, roles, existed_employee);
                await _employeeRepository.AddAsync(existed_employee);
            }

            var result = EmployeeMapper.MapFromModel(existed_employee, true);

            return (EmployeeResponse)result;
        }
    }
}
