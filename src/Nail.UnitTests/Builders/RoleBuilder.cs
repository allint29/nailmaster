﻿using Nail.Core.Domain.Administrations;
using System;
using System.Collections.Generic;
using System.Text;

namespace Nail.UnitTests.Builders
{
    public static class RoleBuilder
    {
        static private List<Role> Roles = new List<Role>()
        {
            new Role()
                {
                    Id = Guid.Parse("53729686-a368-4eeb-8bfa-cc69b6050d02"),
                    Name = "Admin",
                    Description = "Администратор",
                },
            new Role()
                {
                    Id = Guid.Parse("53729686-2222-3333-8bfa-cc69b6050d02"),
                    Name = "PartnerManager",
                    Description = "Партнерский менеджер"
                }};
        public static Role CreateBaseAdminRolee()
        {
            return Roles[0];
        }
    }
}
