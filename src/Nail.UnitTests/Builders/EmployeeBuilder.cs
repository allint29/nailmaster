﻿using Nail.Core.Domain.Administrations;
using System;
using System.Collections.Generic;
using System.Text;

namespace Nail.UnitTests.Builders
{
    public static class EmployeeBuilder
    {
        public static Guid EmployeeGuid => Guid.Parse("451533d5-d8d5-4a11-9c7b-eb9f14e1a32f");
        public static Guid BadEmployeeGuid => Guid.Parse("11111111-d8d5-4a11-1111-eb9f14e1a111");
        public static Guid RoleAdminGuid => Guid.Parse("53729686-a368-4eeb-8bfa-cc69b6050d02");
        public static Employee CreateBaseEmployee()
        {
            return new Employee()
            {
                Id = EmployeeGuid,
                Email = "owner@somemail.ru",
                FirstName = "Иван",
                LastName = "Сергеев",
                RoleIds = new List<Guid>() { RoleAdminGuid },
                Roles = new List<Role>() {
                    new Role {
                        Id = RoleAdminGuid,
                        Name = "Admin" ,
                        Description = "Description Admin",
                        EmployeeIds = new List<Guid>(),
                        Employees = new List<Employee>()
                    }
                }
            };
        }

        public static Employee WithoutRoles(this Employee employee)
        {
            employee.RoleIds = new List<Guid>();
            employee.Roles = new List<Role>();
            return employee;
        }

        
    }
}
