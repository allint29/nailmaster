﻿using Nail.Core.Domain.Administrations;
using Nail.Core.Interfaces.IRepositories;
using Nail.Core.Application.Services;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;
using Nail.Core.Application.Exceptions;
using System.Threading.Tasks;
using FluentAssertions;
using AutoFixture;
using AutoFixture.AutoMoq;
using Nail.Core.Interfaces.IServices;
using Nail.Core.Interfaces.IGateways;
using Nail.Core.Application.DtoModels.Employees;
using Nail.UnitTests.Builders;

namespace UnitTests.Nail.Core.Application.Services
{

    public class DeleteEmployeeAsyncTest
    {
        private readonly Mock<IRepository<Employee>> _employeeRepositoryMock;
        private readonly Mock<IRepository<Role>> _roleRepositoryMock;
        private readonly EmployeeService _employeeService;
        private readonly Mock<INotificationGateway> _notificationGateway;
        //private readonly Employee _employeeFirstFullInfo;

        public DeleteEmployeeAsyncTest()
        {
            var fixture = new Fixture().Customize(new AutoMoqCustomization());
            //создаю заглушку реппозитория
            _employeeRepositoryMock = fixture.Freeze<Mock<IRepository<Employee>>>();
            _roleRepositoryMock = fixture.Freeze<Mock<IRepository<Role>>>();
            _notificationGateway = fixture.Freeze<Mock<INotificationGateway>>();
            //создаю объект сервиса работника
            _employeeService = fixture.Build<EmployeeService>().OmitAutoProperties().Create();

        }

        [Fact]
        private async void DeleteEmployeeAsync_BadGuid_ShouldBeEntityFoundExeption()
        {
            //Arrange
            var Id = EmployeeBuilder.BadEmployeeGuid;            
            //создаю объект, который ожидаю получить с реппозитория
            Employee expected_employee = null;  
            //настраиваю как должен работать - достать работника по Id
            _employeeRepositoryMock.Setup(e => e.GetByIdAsync(Id)).ReturnsAsync(expected_employee);

            //Act
            Func<Task> act = async () => await _employeeService.DeleteEmployeeAsync(Id);

            //Assert
            await act.Should().ThrowAsync<EntityNotFoundException>();
            //await Assert.ThrowsAsync<EntityNotFoundException>(() => employeeService.DeleteEmployeeAsync(Id));
         
        }
        //[Fact]
        //private async void DeleteEmployeeAsync_EmployeeSuccessDeleted_ShouldBeEntityFoundExeption()
        //{
        //    //Arrange
        //    var Id = Guid.Parse("451533d5-d8d5-4a11-9c7b-eb9f14e1a32f");
        //
        //    //создаю объект, который ожидаю получить с реппозитория
        //    Employee expected_employee = new Employee()
        //    {
        //        Id = Guid.Parse("451533d5-d8d5-4a11-9c7b-eb9f14e1a32f"),
        //        Email = "owner@somemail.ru",
        //        FirstName = "Иван",
        //        LastName = "Сергеев",
        //        RoleIds = new List<Guid>(),
        //        Roles = new List<Role>()
        //    };
        //
        //    //var expected_role = new Role()
        //    //{
        //    //    Id = Guid.Parse("53729686-a368-4eeb-8bfa-cc69b6050d02"),
        //    //    Name = "Admin"
        //    //};
        //
        //    //настраиваю как должен работать - достать работника по Id
        //    _employeeRepositoryMock.Setup(e => e.GetByIdAsync(Id)).ReturnsAsync(expected_employee);
        //    //_roleRepositoryMock.Setup(r => r.GetByIdAsync(Id)).ReturnsAsync(expected_role);
        //
        //    //Act
        //    Func<Task> act = async () => await _employeeService.DeleteEmployeeAsync(Id);
        //    //Assert
        //    _notificationGateway.Verify(x => x.SendNotificationToEmployeeAsync(expected_employee.Id, It.IsAny<string>()), Times.Once);
        //}

        [Fact]
        private async void DeleteEmployeeAsync_EmployeeSuccessDeletedFact_ShouldBeEntityFoundExeption()
        {
            //Arrange
            var Id = EmployeeBuilder.EmployeeGuid;

            //создаю объект, который ожидаю получить с реппозитория
            Employee expected_employee = EmployeeBuilder.CreateBaseEmployee();

            //настраиваю как должен работать - достать работника по Id
            _employeeRepositoryMock.Setup(e => e.GetByIdAsync(Id)).ReturnsAsync(expected_employee);

            //Act
            var result = await _employeeService.DeleteEmployeeAsync(Id);
            //Assert
            Assert.IsType<EmployeeResponse>(result);
        }

        //Не работает
        //[Fact]
        //private async void DeleteEmployeeAsync_GoodGuid_ShouldBeEntityFoundExeption()
        //{
        //    //Arrange
        //    var Id = Guid.Parse("451533d5-d8d5-4a11-9c7b-eb9f14e1a32f");
        //    //создаю заглушку реппозитория
        //    var mockEmployeeRepository = new Mock<IRepository<Employee>>();
        //    var mockRoleRepository = new Mock<IRepository<Role>>();
        //    //создаю объект, который ожидаю получить с реппозитория
        //    Employee expected_employee = new Employee()
        //    {
        //        Id = Guid.Parse("451533d5-d8d5-4a11-9c7b-eb9f14e1a32f"),
        //        Email = "owner@somemail.ru",
        //        FirstName = "Иван",
        //        LastName = "Сергеев",
        //        RoleIds = new List<Guid>(),
        //        Roles = new List<Role>()
        //    };
        //
        //    var expected_role = new Role()
        //    {
        //        Id = Guid.Parse("53729686-a368-4eeb-8bfa-cc69b6050d02"),
        //        Name = "Admin"
        //    };
        //
        //
        //    //настраиваю как должен работать - достать работника по Id
        //    mockEmployeeRepository.Setup(e => e.GetByIdAsync(Id)).ReturnsAsync(expected_employee);
        //    mockRoleRepository.Setup(r => r.GetByIdAsync(Id)).ReturnsAsync(expected_role);
        //
        //    //создаю объект сервиса работника
        //    //var employeeService = new EmployeeService(mockRoleRepository.Object, mockEmployeeRepository.Object);
        //
        //    //Act
        //
        //
        //    //Assert
        //    //await Assert.ThrowsAsync<EntityNotFoundException>(() => employeeService.DeleteEmployeeAsync(Id));
        //    mockEmployeeRepository.Verify(m => m.DeleteAsync(expected_employee), Times.Once());
        //    //mockEmployeeRepository.Verify(m => m.DeleteAsync(It.IsAny<Employee>()), Times.Never());
        //
        //}
    }
}
