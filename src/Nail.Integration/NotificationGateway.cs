﻿using Nail.Core.Interfaces.IGateways;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Nail.Integration
{
    public class NotificationGateway
        : INotificationGateway
    {
        public Task SendNotificationToEmployeeAsync(Guid employeeId, string message)
        {
            //Код, который вызывает сервис отправки уведомлений партнеру

            return Task.CompletedTask;
        }

    }
}
