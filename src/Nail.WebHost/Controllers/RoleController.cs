﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Nail.Core.Domain.Administrations;
using Nail.Core.Interfaces.IRepositories;
using Microsoft.AspNetCore.Mvc;
using Nail.Core.Application.DtoMappers;
using Nail.Core.Application.DtoModels.Employees;
using Nail.Core.Application.DtoModels.Roles;
using Nail.Core.Interfaces.IServices;
using Nail.Core.Application.Exceptions;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Роли сотрудников
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class RolesController
        : ControllerBase
    {
        private readonly IRoleService _roleService;
        public RolesController(IRoleService roleService)
        {
            _roleService = roleService;
        }

        /// <summary>
        /// Получить все доступные роли сотрудников
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<RoleResponse>> GetRolesAsync()
        {
            var roles = await _roleService.GetRolesAsync();
            return roles;
        }

        /// <summary>
        /// Получить данные роли по Id
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<RoleResponse>> GetRoleByIdAsync(Guid id)
        {
            RoleResponse role = null;
            try
            {
                role = await _roleService.GetRoleByIdAsync(id);
            }
            catch(EntityNotFoundException ex)
            {
                return NotFound(ex);
            } 
            return role;
        }


        /// <summary>
        /// Создание записи о роли
        /// </summary>
        /// <param name="request">Класс запроса для создания записи о роли или их изменения</param>
        /// <returns>Данные о покупателе</returns>
        [HttpPost]
        public async Task<ActionResult<RoleResponse>> CreateRoleAsync(CreateOrEditRoleRequest request)
        {                
            var role = await _roleService.UpdateRoleAsync(request);

            return CreatedAtAction(nameof(CreateRoleAsync), new { id = role.Id }, role.Id);
        }


        /// <summary>
        /// Изменение данных роли
        /// </summary>
        /// <param name="id">Уникальный идентификатор роли</param>
        /// <param name="request">Класс запроса для создания записи о роли или их изменения</param>
        /// <returns></returns>
        [HttpPut("{id:guid}")]
        public async Task<IActionResult> EditRoleAsync(Guid id, CreateOrEditRoleRequest request)
        {
            if (request.Id == Guid.Empty || request.Id == null)
                request.Id = id;

            await _roleService.UpdateRoleAsync(request);

            return Ok();
        }

        /// <summary>
        /// Удалить данные о роли
        /// </summary>
        /// <param name="id">Уникальный идентификатор роли</param>
        /// <returns></returns>
        [HttpDelete("{id:guid}")]
        public async Task<IActionResult> DeleteRoleAsync(Guid id)
        {
            await _roleService.DeleteRoleAsync(id);
            return Ok();
        }

    }
}
