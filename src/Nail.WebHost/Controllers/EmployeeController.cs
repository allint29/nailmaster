﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Nail.Core.Domain.Administrations;
using Nail.Core.Interfaces.IRepositories;
using Microsoft.AspNetCore.Mvc;
using Nail.Core.Application.DtoMappers;
using Nail.Core.Application.DtoModels.Employees;
using Nail.Core.Application.DtoModels.Roles;
using Nail.Core.Interfaces.IServices;
using Nail.Core.Application.Exceptions;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Сотрудники
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class EmployeesController
        : ControllerBase
    {
        private readonly IEmployeeService _employeeService;

        public EmployeesController(IEmployeeService employeeService)
        {
            _employeeService = employeeService;
        }

        /// <summary>
        /// Получить данные всех сотрудников
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<EmployeeResponse>> GetEmployeesAsync()
        {
            var employees = await _employeeService.GetEmployeesAsync();

            //await 

            return employees;
        }

        /// <summary>
        /// Получить данные сотрудника по Id
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<EmployeeResponse>> GetEmployeeByIdAsync(Guid id)
        {
            EmployeeResponse employee = null;
            try
            {
                employee = await _employeeService.GetEmployeeByIdAsync(id);
            }
            catch (EntityNotFoundException ex)
            {
                return NotFound(ex);
            }
            return employee;
        }

        /// <summary>
        /// Создание записи о работнике
        /// </summary>
        /// <param name="request">Класс запроса для создания записи о покупателе или их изменения</param>
        /// <returns>Данные о покупателе</returns>
        [HttpPost]
        public async Task<ActionResult<EmployeeResponse>> CreateEmployeeAsync(CreateOrEditEmployeeRequest request)
        {
            var employee = await _employeeService.UpdateEmployeeAsync(request);

            return CreatedAtAction(nameof(CreateEmployeeAsync), new { id = employee.Id }, employee.Id);
        }

        /// <summary>
        /// Изменение данных работника
        /// </summary>
        /// <param name="id">Уникальный идентификатор работника</param>
        /// <param name="request">Класс запроса для создания записи о работнике или их изменения</param>
        /// <returns></returns>
        [HttpPut("{id:guid}")]
        public async Task<IActionResult> EditEmployeeAsync(Guid id, CreateOrEditEmployeeRequest request)
        {
            if (request.Id == Guid.Empty || request.Id == null)
                request.Id = id;

            await _employeeService.UpdateEmployeeAsync(request);

            return Ok();
        }

        /// <summary>
        /// Удалить данные о работнике
        /// </summary>
        /// <param name="id">Уникальный идентификатор работника</param>
        /// <returns></returns>
        [HttpDelete("{id:guid}")]
        public async Task<IActionResult> DeleteEmployeeAsync(Guid id)
        {
            await _employeeService.DeleteEmployeeAsync(id);
            return Ok();
        }

        /// <summary>
        /// Удалить роль у работнника
        /// </summary>
        /// <param name="id">Уникальный идентификатор работника</param>
        /// <returns></returns>
        [HttpPut("remove_role/{role_id:guid}/{employee_id:guid}")]
        public async Task<IActionResult> DeleteRoleFromEmployeeAsync(Guid role_id, Guid employee_id)
        {
            var result = await _employeeService.RemoveRoleFromEmployeeAsync(role_id, employee_id);

            return Ok();
        }

        /// <summary>
        /// Удалить роль у работнника
        /// </summary>
        /// <param name="id">Уникальный идентификатор работника</param>
        /// <returns></returns>
        [HttpPut("set_role/{role_id:guid}/{employee_id:guid}")]
        public async Task<IActionResult> SetRoleFromEmployeeAsync(Guid role_id, Guid employee_id)
        {
            var result = await _employeeService.SetRoleToEmployeeAsync(role_id, employee_id);

            return Ok();
        }

    }
}
