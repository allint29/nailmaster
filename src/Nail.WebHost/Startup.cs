﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Nail.Core.Domain.Administrations;
using Nail.Core.Interfaces.IRepositories;
using Nail.DataAccess.Data;
using Nail.DataAccess.Repositories;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Options;
using Nail.DataAccess.Abstractions;
using Nail.DataAccess.EntityFramework;
using Microsoft.EntityFrameworkCore;
using Nail.Core.Interfaces.IServices;
using Nail.Core.Application.Services;
using Npgsql;
using Nail.Core.Interfaces.IProviders;
using Nail.Core.Application.Providers;
using Nail.Core.Interfaces.IGateways;
using Nail.Integration;
using Elastic.Apm.NetCoreAll;
using Microsoft.AspNetCore.Cors;

namespace WebHost
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers().AddMvcOptions(x =>
                x.SuppressAsyncSuffixInActionNames = false);

            //services.AddScoped<IDbInitializer, FakeDataInitializer>();
            services.AddScoped<IDbInitializer, EfDbInitializer>();
            services.AddScoped<INotificationGateway, NotificationGateway>();
            services.AddScoped(typeof(IRepository<>), typeof(EfRepository<>));
            services.AddScoped<IEmployeeService, EmployeeService>();
            services.AddScoped<IRoleService, RoleService>();

            
            //services.AddScoped(typeof(IRepository<Employee>), (x) =>
            //    new InMemoryRepository<Employee>(FakeDataFactory.Employees.OfType<Employee>().ToList()));
            //services.AddScoped(typeof(IRepository<Role>), (x) =>
            //    new InMemoryRepository<Role>(FakeDataFactory.Roles.OfType<Role>().ToList()));
            //var builder = new NpgsqlConnectionStringBuilder(
            //    Configuration.GetConnectionString("NpgDb"))
            //{
            //    Password = Configuration["Password"]
            //};
            //services.AddDbContext<SomeContext>(opt => opt.UseInMemoryDatabase(), ServiceLifetime.Singleton);
            services.AddTransient<ICurrentDateTimeProvider, CurrentDateTimeProvider>();
            services.AddDbContext<DataContext>(
                x =>
                    {
                        //x.UseSqlite("Filename=PromoCodeFactoryDb.sqlite");
                        //x.UseLazyLoadingProxies();
                        //x.UseNpgsql(builder.ToString());
                        //x.UseInMemoryDatabase()
                        x.UseNpgsql(Configuration.GetConnectionString("NpgDb"));
                        x.EnableSensitiveDataLogging();
                    }
                    , 
                ServiceLifetime.Singleton
                );


            services.AddOpenApiDocument(options =>
            {
                options.Title = "PromoCode Factory API Doc";
                options.Version = "1.0";
            });

            services.AddCors();

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, IDbInitializer initializer)
        {
            app.UseAllElasticApm(Configuration);

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            app.UseOpenApi();
            app.UseSwaggerUi3(x =>
            {
                x.DocExpansion = "list";
            });

            app.UseHttpsRedirection();

            app.UseRouting();

            // подключаем CORS
            app.UseCors(builder => builder.AllowAnyOrigin());

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

            initializer.InitializeDb();

        }
    }
}
